package reports;

public class Car {
	// 필드
	Tire tires[]= {								// 객체 배열(모두 구르기 위함)
		new Tire("앞 왼쪽",4),
		new Tire("앞 오른쪽",2),
		new Tire("뒤 왼쪽",4),
		new Tire("뒤 오른쪽",3)
	};
	int punk_count =0;							// 펑크_카운트(동시 펑크를 위함)
	boolean check[] = {true,true,true,true};	// 어디 바퀴가 터졌는지 체크(동시 펑크를 위함)
	
	// 메소드
	int run() {
		System.out.println("[자동차가 달립니다.]");
		for(int i=0;i<4;i++) {					//4개 바퀴 동작
			check[i]=tires[i].roll();				// 바퀴 돌아감
			if(check[i] == false)					// 만약 터진 바퀴가 있으면
				punk_count++;							// 카운트 증가
		}
		return 0;									// 객체 종료
	}
	void stop() {
		System.out.println("[자동차가 멈춥니다.]");
	}
}
