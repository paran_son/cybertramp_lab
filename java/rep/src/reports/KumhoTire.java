package reports;

public class KumhoTire extends Tire{
	// 생성자
	public KumhoTire(String location, int maxRotation) {
		super(location, maxRotation);
	} 
	
	@Override
	public boolean roll() {
		++accumulatedRotation;
		System.out.println(location + "Tire 수명: "+(maxRotation-accumulatedRotation)+" 회");
		if(accumulatedRotation<maxRotation) {
			return true;
		}
		else {
			System.out.println("*** "+location+" 금호타이어 펑크 ***");
			return false;
		}
	}
	// 메소드
}
