
public class EX2_TryCatchFinallyRuntimeExceptionEx {
	public static void main(String[] args) {
		String data1 = null;
		String data2 = null;
		try {
			data1 = args[0];
			data2 = args[1];
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("실행 매개값의 수가 부족합니다.");
			return;		// 프로그램이 종료되어야 하나 finally를 실행을 하고 종료 됨.
		}
		try {
			int val1 = Integer.parseInt(data1);
			int val2 = Integer.parseInt(data2);
			int res = val1 + val2;
			System.out.println(data1+"+"+data2+"="+res);
		}
		catch(NumberFormatException e){
			System.out.println("숫자로 변환할 수 없습니다.");
		}
		finally {
			System.out.println("다시 실행하세요.");
		}
	}
}
