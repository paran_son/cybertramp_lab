package EX3_interface_extends;

public interface InterfaceC extends InterfaceA, InterfaceB{
	// InterfaceA와 InterfaceB를 상속(물려받음)하므로 methodA(), methodB()를 사용 가능
	public void methodC();
	
}
