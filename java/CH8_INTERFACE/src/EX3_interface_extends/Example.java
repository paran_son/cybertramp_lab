package EX3_interface_extends;

public class Example {
	public static void main(String[] args) {
		ImplementationC impl = new ImplementationC();
		
		InterfaceA ia = impl;		// ia는 InterfaceA의 methodA()만 가능
		ia.methodA();
		System.out.println();
		
		InterfaceB ib = impl;		// ib는 InterfaceB의 methodB()만 가능
		ib.methodB();
		System.out.println();
	
		InterfaceC ic = impl;		// ic는 InterfaceC의 methodC() 포함 methodA(), methodB() 가능
		ic.methodA();
		ic.methodB();
		ic.methodC();
	}
	
}
