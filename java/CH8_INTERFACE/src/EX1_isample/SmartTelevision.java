package EX1_isample;

public class SmartTelevision implements RemoteControl, Searchable{
	private int volume;
	
	public void turnOn() {
		System.out.println("TV ON");
	}
	public void turnOff() {
		System.out.println("TV OFF");
	}
	public void setVolume(int volume) {
		if(volume>RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}
		else if(volume<RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		}
		else {
			this.volume = volume;
		}
		System.out.println("현재 TV 볼륨 : "+volume);
	}
	public void search(String url) {
		System.out.println(url + "을 검색합니다.");
	}
	public void changeBattery() {
		System.out.println("건전지를 교환합니다.");
	}
}
