package EX1_isample;

public class RemoteControlEx {
	public static void main(String args[]) {
		/*
		RemoteControl rc = null;	// rc 인터페이스 변수 
		rc = new Television();
		rc.turnOn();
		rc.setVolume(3);
		rc.setMute(true);
		rc.turnOff();
		System.out.println("====");
		rc = new Audio();
		rc.turnOn();
		rc.setVolume(5);
		rc.setMute(true);
		rc.turnOff();
		*/
		SmartTelevision tv = new SmartTelevision();
		RemoteControl rc = tv;
		Searchable searchable = tv;
		
		rc.turnOn();
		rc.setVolume(5);
		rc.setMute(true);
		searchable.search("www.example.com");
		rc.turnOff();
		RemoteControl.changeBattery();		// 정적
	}
}