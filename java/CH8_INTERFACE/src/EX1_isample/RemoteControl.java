package EX1_isample;

public interface RemoteControl {
	/* 상수필드 */
	// 자동적으로 컴파일 과정에서 상수로 자동 처리됨 public static final int
	public int MAX_VOLUME = 10;
	public int MIN_VOLUME = 0;
	
	/* 추상메소드 */
	public void turnOn();
	public void turnOff();
	public void setVolume(int volume);
	
	/* 디폴트 메소드(자바8) */
	// 인터페이스에 기능을 추가 하는 것
	// 이게 없으면 객체와 개발코드 모두 수정해야하는 번거로움을 줄이기 위해
	default void setMute(boolean mute) {
		if(mute == true) {
			System.out.println("무음 처리합니다.");
		}
		else {
			System.out.println("무음 해제합니다.");
		}
	}
	/* 정적메소드(자바8) */
	static void changeBattery() {
		System.out.println("건전지를 교환합니다.");
	}

}
