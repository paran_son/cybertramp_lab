package ch_7;

public class PromotionEx {
	public static void main(String[] args) {
		// PromotionEx_A
		// B는 A에 상속
		// C는 A에 상속
		// D는 B에 상속
		// E는 C에 상속
		
		PromotionEx_B b = new PromotionEx_B();
		PromotionEx_C c = new PromotionEx_C();
		PromotionEx_D d = new PromotionEx_D();
		PromotionEx_E e = new PromotionEx_E();
		
		PromotionEx_A a1 = b;
		PromotionEx_A a2 = c;
		PromotionEx_A a3 = d;
		PromotionEx_A a4 = e;
		
		PromotionEx_B b1 = d;
		PromotionEx_C c1 = e;
		
		//PromotionEx_B b3 = e;			// 컴파일 에러(상속관계가 아님)	E -> C -> A
		//PromotionEx_C c2 = d;			// 컴파일 에러(상속관계가 아님)	D -> B -> A
		
	}
}
