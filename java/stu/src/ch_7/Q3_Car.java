package ch_7;

public class Q3_Car {
	// 필드
	Q3_Tire frontLeftTire = new Q3_Tire("앞 왼쪽",6);
	Q3_Tire frontRighttire = new Q3_Tire("앞 오른쪽",2);
	Q3_Tire backLeftTire = new Q3_Tire("뒤 왼쪽",3);
	Q3_Tire backRightTire = new Q3_Tire("뒤 오른쪽",4);
	
	// 메소드
	int run() {
		System.out.println("[자동차가 달립니다.]");
		if(frontLeftTire.roll() == false) {
			stop();
			return 1;
		}
		if(frontRighttire.roll() == false) {
			stop();
			return 2;
		}
		if(backLeftTire.roll() == false) {
			stop();
			return 3;
		}
		if(backRightTire.roll() == false) {
			stop();
			return 4;
		}
		return 0;
	}
	void stop() {
		System.out.println("[자동차가 멈춥니다.]");
	}
}
