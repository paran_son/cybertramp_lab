package ch_7;

public class Q3_HankookTire extends Q3_Tire{
	// 생성자
	public Q3_HankookTire(String location, int maxRotation) {
		super(location, maxRotation);
	}

	@Override
	public boolean roll() {
		++accumulatedRotation;
		if(accumulatedRotation<maxRotation) {
			System.out.println(location + "Tier 수명: "+(maxRotation-accumulatedRotation)+" 회");
			return true;
		}
		else {
			System.out.println("*** "+location+" 한국타이어 펑크 ***");
			return false;
		}
		
	}
	// 메소드
	
}
