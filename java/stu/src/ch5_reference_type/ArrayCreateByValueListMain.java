/* 강조 하신 실습문제 */
package ch5_reference_type;

public class ArrayCreateByValueListMain {

	public static void main(String[] args) {
		int[] scores;
		
		// 현재 메소드 내에서 사용
		scores = new int[] {83, 90, 87};
		int sum1 = 0;
		for(int i=0;i<3;i++) {
			sum1 += scores[i];
		}
		System.out.println("총합 : "+sum1);
		
		// 호출해서 사용
		int sum2 = ArrayCreateByValueListStatic.add(new int[] {83,90,87});			// 호출
		/*
		 * int sum2 = ArrayCreateByValueListStatic.add(scores);						// 동일 함
		 */
		System.out.println("Static을 사용한 총합 : "+sum2);
		System.out.println();
		
		// 객체 생성해서 사용
		ArrayCreateByValueListObject arrObj = new ArrayCreateByValueListObject();	// 객체 생성
		int sum3 = arrObj.add(new int[] {15,30,50});
		
		System.out.println("객체를 사용한 총합 : "+sum3);
		System.out.println();
	
	}

}
