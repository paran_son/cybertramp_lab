package ch6_class;

public class Korean {
	String nation = "대한민국";
	String name;
	String ssn;
	
	public Korean(String name, String ssn) {
		this.name = name;	// 필드와 매개변수를 구분하기 위해 사용
		this.ssn = ssn;
	}
}
