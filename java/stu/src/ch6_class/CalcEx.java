package ch6_class;

public class CalcEx {
	public static void main(String[] args) {
		
		double res1 = 10 * 10 * Calc.pi;	// 클래스의 정적 멤버인 필드 pi 호출
		int res2 = Calc.plus(10, 5);		// 클래스의 정적 멤버인 메소드 plus 호출
		int res3 = Calc.minus(10,5);		// 클래스의 정적 멤버인 메소드 minus 호출
		
		System.out.println("res1의 값 : "+res1+"\nres2의 값 : "+res2+"\nres3의 값 : "+res3);
	}
}
