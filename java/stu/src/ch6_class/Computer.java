package ch6_class;

public class Computer {
	/* 메소드 */
	int sum1(int[] values) {		// 매개변수를 모를 경우
		int sum = 0;
		for(int i=0; i<values.length;i++) {
			sum+=values[i];
		}
		return sum;
	}
	
	int sum2(int ... values) {		// 매개변수를 모를 경우
		int sum = 0;
		for(int i=0;i<values.length;i++) {
			sum+=values[i];
		}
		return sum;
	}
}
