package ch6_class;

public class ComputerEx {
	public static void main(String[] args) {
		Computer myCom = new Computer();
		/* 배열 생성 하고 넘김 */
		int[] val1 = {1,2,3};
		int res1 = myCom.sum1(val1);						// 만들고 넘김
		System.out.println("result1 : "+res1);
		
		int res2 = myCom.sum1(new int[] {1,2,3,4,5});		// 만들면서 넘김
		System.out.println("result2 : "+res2);
		
		/* 배열 생성 안하고 리스트만 넘김 */
		int res3 = myCom.sum2(1,2,3);						// 매개변수 3개 넘김
		System.out.println("result3 : "+res3);
		
		int res4 = myCom.sum2(1,2,3,4,5);					// 매개변수 5개 넘김
		System.out.println("result4 : "+res4);
	}
}
