package ch6_class;

public class CalculatorEx {
	public static void main(String[] args) {
		/* 클래스로 부터 myCalc 객체 생성 */
		Calculator myCalc = new Calculator();
		myCalc.powerOn();
		
		int res1 = myCalc.plus(5,6);			// myCalc의 plus 메소드 호출
		System.out.println("result1 : "+res1);
		
		byte x = 10;
		byte y = 4;
		double res2 = myCalc.divide(x,y);		// myCalc의 divide 메소드 호출
		System.out.println("result2 : "+res2);
		
		myCalc.powerOff();
		
		System.out.println();
		
		/* 사각형 넓이 */
		double res3 = myCalc.areaRectangle(10);			// 정사각형
		double res4 = myCalc.areaRectangle(10, 20);		// 직사각형
		System.out.println("정사각형 넓이 : "+res3);
		System.out.println("직사각형 넓이 : "+res4);
	}
}
