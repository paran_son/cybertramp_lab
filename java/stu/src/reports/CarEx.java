package reports;

public class CarEx {
	public static void main(String[] args) {
		Car car = new Car();					// car 객체 선언
		for(int i=0;i<=5;i++) {					// 달리는 횟수
			car.run();								// 자동차가 동작
			while(car.punk_count!=0) {				// punk_count 존재 하면 반복
				if(car.check[0] == false) {							// 앞 왼쪽 터짐
					car.tires[0] = new HankookTire("앞 왼쪽", 5);			// 타이어 교체
					System.out.println("앞 왼쪽 한국타이어로 교체 => "+(car.tires[0].maxRotation-1)+"회 로 변경됨");
					car.punk_count--;									// punk_count 감소
				}
				if(car.check[1] == false) {							// 앞 오른쪽 터짐
					car.tires[1] = new KumhoTire("앞 오른쪽", 2);			// 타이어 교체
					System.out.println("앞 오른쪽 금호타이어로 교체 => "+(car.tires[1].maxRotation-1)+"회 로 변경됨");
					car.punk_count--;									// punk_count 감소
				}
				if(car.check[2] == false) { 						// 뒤 왼쪽 터짐
					car.tires[2] = new HankookTire("뒤 왼쪽", 4);			// 타이어 교체
					System.out.println("뒤 왼쪽 한국타이어로 교체 => "+(car.tires[2].maxRotation-1)+"회 로 변경됨");
					car.punk_count--;									// punk_count 감소
				}
				if(car.check[3] == false) {							// 뒤 오른쪽 터짐
					car.tires[3] = new KumhoTire("뒤 오른쪽", 3);			// 타이어 교체
					System.out.println("뒤 오른쪽 금호타이어로 교체 => "+(car.tires[3].maxRotation-1)+"회 로 변경됨");
					car.punk_count--;									// punk_count 감소
				}
			}
			System.out.println("------------------------------");
		}
			
	}
}

