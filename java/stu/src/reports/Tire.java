package reports;

public class Tire {
	// 필드
	public int maxRotation;
	public int accumulatedRotation;
	public String location;
	
	// 생성자
	public Tire(String location, int maxRotation) {
		this.location = location;
		this.maxRotation = maxRotation;
	}
	
	// 메소드
	public boolean roll() {		// 바퀴 회전
		System.out.println(location + "Tire 수명:"+(maxRotation-accumulatedRotation)+" 회");
		++accumulatedRotation;		// 회전 수 증가
		if(accumulatedRotation<maxRotation+1) {
			return true;
		}
		else {
			System.out.println("*** "+location+" 펑크"+" ***");
			return false;
		}
	}
}
