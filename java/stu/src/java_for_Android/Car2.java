package java_for_Android;

public class Car2 {
	String color;
	int speed;
	
	static int carCount = 0;			// 정적 필드(클래스 자체에서 사용되는 변수)
	final static int MAXSPEED = 200;	// 상수 필드
	final static int MINSPEED = 0;		// 상수 필드
	
	static int currentCarCount() {		// 정적 메소드(인스턴스 없이 클래스명.메소드명()으로 호출)
		return carCount;
	}
	
	public Car2(String color, int speed){
	    this.color = color;
	    this.speed = speed;
	    carCount++;
	}
	
	int getSpeed() {
		return speed;
	}
	
	void upSpeed(int value) {
		if(speed + value >= 200)
			speed = 200;
		else
			speed = speed + value;
	}
	void downSpeed(int value) {
		if(speed - value <= 0)
			speed = 0;
		else
			speed = speed - value;
	}
	String getColor() {
		return color;
	}
}
