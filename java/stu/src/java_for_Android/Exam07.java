package java_for_Android;

public class Exam07 {
	public static void main(String args[]) {
		Car myCar1 = new Car("빨강",0);
		
		myCar1.upSpeed(50);
		System.out.println("자동차1의 색상은 "+myCar1.getColor()+"이며, 속도는 "+myCar1.getSpeed()+"km입니다.");
		myCar1.upSpeed(100);
		System.out.println("자동차1의 색상은 "+myCar1.getColor()+"이며, 속도는 "+myCar1.getSpeed()+"km입니다.");
		myCar1.upSpeed(200);
		System.out.println("자동차1의 색상은 "+myCar1.getColor()+"이며, 속도는 "+myCar1.getSpeed()+"km입니다.");
		myCar1.downSpeed(70);
		System.out.println("자동차1의 색상은 "+myCar1.getColor()+"이며, 속도는 "+myCar1.getSpeed()+"km입니다.");
		
	}
}
