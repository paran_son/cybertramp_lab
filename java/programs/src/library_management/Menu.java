/* 도서 관리 프로그램 */
package library_management;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Menu {
	public static void main(String[] args) {
		
		Date dt = new Date();		// Date 클래스 생성자
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");	//SimpleDateFormat 클래스 생성자
		
		
		Scanner scan = new Scanner(System.in);
		String version = " v0.1";
		String in;
		System.out.println("[ Library_management_Program ]"+version);
		System.out.println("=============================");
		System.out.println("  Date : "+ sdf.format(dt));
		
		System.out.println("=============================");
		System.out.println("1. [r]egistration or management");
		System.out.println("2. [b]ook return or loan");
		System.out.println("3. [s]earch");
		System.out.println("4. [e]nvironment settings");
		System.out.println("5. [q]uit");
		System.out.println("=============================");
		System.out.println("Please choose Menu. ");
		System.out.println("Choose : ");
		in = scan.nextLine();
		// 반납대출
		// 등록관리
		// 검색
		// 환경설정
	}
}
