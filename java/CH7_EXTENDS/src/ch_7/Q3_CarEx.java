package ch_7;

public class Q3_CarEx {
	public static void main(String[] args) {
		Q3_Car car = new Q3_Car();
		
		for(int i=1;i<=5;i++) {
			int problemLocation = car.run();
			
			switch(problemLocation) {
			case 1:
				System.out.println("앞 왼쪽 한국타이어로 교체");
				car.frontLeftTire = new Q3_HankookTire("앞 왼쪽", 15);
				break;
			
			case 2:
				System.out.println("앞 오른쪽 금호타이어로 교체");
				car.frontRighttire = new Q3_KumhoTire("앞 오른쪽", 13);
				break;
			
			case 3:
				System.out.println("뒤 왼쪽 한국타이어로 교체");
				car.backLeftTire = new Q3_HankookTire("뒤 왼쪽", 14);
				break;
			case 4:
				System.out.println("뒤 오른쪽 금호타이어로 교체");
				car.backRightTire = new Q3_KumhoTire("뒤 오른쪽", 17);
				break;
			}
			System.out.println("------------------------------");
		}
	}
}
