package ch_7;

public class Q6_AnimalEx {
	public static void main(String[] args) {
		Q6_Dog dog = new Q6_Dog();
		Q6_Cat cat = new Q6_Cat();
		
		dog.sound();	// 안좋은 예
		cat.sound();	// 안좋은 예
		System.out.println("----");
		
		Q6_Animal animal = null;
		animal = new Q6_Dog();
		animal.sound();
		animal = new Q6_Cat();
		animal.sound();
		
		System.out.println("----");
		
		animalSound(new Q6_Dog());		// 좋은 예
		animalSound(new Q6_Cat());		// 좋은 예
		
	}
	public static void animalSound(Q6_Animal animal) {
		animal.sound();
	}
}
