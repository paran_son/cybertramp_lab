package ch_7;

public class Q5_InstanceofExample {
	// 메서드
	public static void method1(Q5_Parent parent) {
		if(parent instanceof Q5_Child) {
			Q5_Child child = (Q5_Child)parent;
			System.out.println("method1 - Child로 변환 성공");
		}
		else {		// parent는 Q5_Child를 참조 하지 않음
			System.out.println("method1 - Child로 변환 실패");
		}
	}
	
	public static void method2(Q5_Parent parent) {	// 강제적으로 진행
		Q5_Child child = (Q5_Child)parent;
		System.out.println("method2 - Child로 변환 성공");
	}
	// 메인
	public static void main(String[] args) {
		Q5_Parent parentA = new Q5_Child();
		method1(parentA);
		method2(parentA);
		
		Q5_Parent parentB = new Q5_Parent();	// 자식에 대한 정보가 없음
		method1(parentB);
		method2(parentB);	// 에외 발생
		
	}
}
