package ch_7;

public class Q4_ChildEx {
	public static void main(String[] args) {
		Q4_Parent parent = new Q4_Child();	// �ڽ� -> �θ�
		parent.field1 = "data1";
		parent.method1();
		parent.method2();
		
		// parent.field2 = "data2";
		// parent.method3();
		
		Q4_Child child = (Q4_Child) parent;
		child.field2 = "yyy";
		child.method3();
	}
}

