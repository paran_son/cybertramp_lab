package ch6_class;

public class Calc {
	
	static double pi = 3.141592;	// 정적 변수 pi
	
	static int plus(int x, int y) {	// 정적 메소드 plus
		return x+y;
	}
	static int minus(int x, int y) {// 정적 메소드 minus
		return x-y;
	}
}
