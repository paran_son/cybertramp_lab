package ch6_class;

public class Calculator {
	/* 메소드 */
	void powerOn(){		// 매개변수 없는 메소드
		System.out.println("전원을 켭니다.");
	}
	void powerOff(){	// 매개변수 없는 메소드
		System.out.println("전원을 끕니다.");
	}
	int plus(int x, int y) {							//매개변수 x,y
		int res = x+y;
		return res;
	}
	double divide(int x, int y) {						// 매개변수 x,y
		double res = (double)x / (double)y;	
		return res;
	}
	double areaRectangle(double width) {				// 매개변수 width
		return width * width;
	}
	double areaRectangle(double width, double height) { // 매개변수 width,height
		return width * height;
	}
}
