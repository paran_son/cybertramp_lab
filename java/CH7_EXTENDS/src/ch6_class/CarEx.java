package ch6_class;

public class CarEx {
	public static void main(String[] args) {
		Car myCar = new Car();	// 객체를 생성
		int t[] = {10,20,30,40,50,60,70,80,90};
		
		System.out.println("제작회사 : "+myCar.company);
		System.out.println("모델명 : "+myCar.model);
		System.out.println("색깔 : "+myCar.color);
		System.out.println("최고속도 : "+myCar.maxSpeed);
		System.out.println("현재속도 : "+myCar.speed);
		System.out.println();
		
		for (int i : t) {
			myCar.speed = i;
			System.out.println("변경된 속도 : "+myCar.speed);
		}
		
		
		
	}
}
