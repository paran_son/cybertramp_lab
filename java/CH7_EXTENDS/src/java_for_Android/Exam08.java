package java_for_Android;

public class Exam08 {
	public static void main(String args[]) {
		Car2 myCar1 = new Car2("빨강",0);
		
		System.out.println("생산된 차의 대수(정적필드) ==> "+Car2.carCount);
		System.out.println("생산된 차의 대수 (정적메소드) ==> "+Car2.currentCarCount());
		System.out.println("차의 최고 제한 속도 ==> "+Car2.MAXSPEED+"\n");
		
		System.out.println("PI 값 : "+Math.PI);
		System.out.println("3의 제곱 : "+Math.pow(3, 5));
	}
}
