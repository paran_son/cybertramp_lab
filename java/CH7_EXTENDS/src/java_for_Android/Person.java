package java_for_Android;

public class Person {
	public String name;
	public int age;
	
	public Person(String s) {
		name = s;
	}
	public Person(String s,int i) {
		name = s;
		age = i;
	}
	public void setAge(int n) {
		age = n;
		n++;
	}
	public String getName() {
		return name;
	}
	public void disp() {
		System.out.println("이름은 "+getName()+"입니다.");
		System.out.println("나이는 "+age+"살 입니다.");
	}
}
