package ch5_reference_type;

public class ArrayCreateByValueListObject {
	public int add(int[] scores) {			// static이 아니기 때문에 호출하려면 객체를 생성하여 사용하여야 함
		int sum = 0;
		for(int i=0;i<3;i++) {
			sum += scores[i];
		}
		return sum;
	}
}
