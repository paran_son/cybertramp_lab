package ch5_reference_type;

public class ArrayCreateByValueListStatic {
	public static int add(int[] scores) {		// static 객체를 만들어서 호출하는게 아님
		int sum = 0;							// 컴파일 단계에서 이미 만들어져 있음
		for(int i=0;i<3;i++) {
			sum += scores[i];
		}
		return sum;
	}
}
