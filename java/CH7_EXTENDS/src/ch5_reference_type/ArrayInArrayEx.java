/* 다차원 배열 예제 */
package ch5_reference_type;

public class ArrayInArrayEx {
	public static void main(String[] args) {
		int[][] mathScores = new int[2][3];
		for(int i=0;i<mathScores.length;i++) {					// mathScores 길이는 2가 됨
			for(int k=0;k<mathScores[i].length;k++) {			// mathScores[0] 길이는 3, mathScores[1] 길이는 3
				System.out.println("mathScores["+i+"]["+k+"]="+mathScores[i][k]);
			}
		}
		System.out.println();
		
		int[][] englishScores = new int[2][];
		englishScores[0] = new int[2];							// englishScores[0] 에 배열 int[2] 
		englishScores[1] = new int[3];							// englishScores[1] 에 배열 int[3]
		for(int i=0;i<englishScores.length;i++) {
			for(int k=0;k<englishScores[i].length;k++) {
				System.out.println("englishScores["+i+"]["+k+"]="+englishScores[i][k]);
			}
		}
		System.out.println();
		
		int[][] javaScores = {{95,80},{92,96,80}};				// javaScores[0]에는 [0] [1] , javaScores[1]에는 [0] [1] [2]
		for(int i=0;i<javaScores.length;i++) {
			for(int k=0;k<javaScores[i].length;k++) {
				System.out.println("javaScores["+i+"]["+k+"]="+javaScores[i][k]);
			}
		}
	}
}
