/* 향상된 for 문 예제  */
package ch5_reference_type;

public class AdvancedForExample {
	public static void main(String[] args) {
		int[] scores = {95, 71, 84, 93, 87};		// scores[5]
		
		int sum = 0;
		for(int score : scores)						// score 변수는 현재 인덱스 위치의 값이 나온다.
			sum = sum+score;						// 누적
		System.out.println("점수 총합 = "+sum+"점");
		
		double avg = (double) sum / scores.length;	// 평균 점수를 실수 형식으로 형변환하면서 평균을 구한다.
		System.out.println("점수 평균 = "+avg+"점");
	}
}
