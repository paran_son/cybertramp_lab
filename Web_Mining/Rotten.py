'''
웹마이닝 - Rotten

'''
# pip install bs4
# pip install requests

import requests
from bs4 import BeautifulSoup


url=[]
Food_keyword_list = []
review_list=[]
score=""

for num in range(1,20):                                                                     # 웹페이지 추출 20번 반복
    url = "https://www.rottentomatoes.com/m/avengers_infinity_war/reviews/?page="+str(num)+"&sort="
    html = requests.get(url)                                                                # html 파일 요청
    soup = BeautifulSoup(html.text, 'html.parser')                                          # 가져온 html 파일을 html.parse
    reviews = soup.findAll("div", attrs={"class":"row review_table_row"})
    for line in reviews:
        review_user = line.find("a",attrs = {"class":"unstyled bold articleLink"}).get_text()
        review_text = line.find("div",attrs = {"class":"the_review"}).get_text()
        review_score = line.find("div",attrs = {"class":"small subtle"})
        polarity =""
        if len(review_score.get_text().split(":"))>1:
            score = review_score.get_text().split(":")[1]
            c = 0
            if score[1].isdigit() == True:
                if len(score) > 2:
                    a = float(score.split("/")[0])
                    b = float(score.split("/")[1])
                    c = a/b
                elif len(score) == 2:
                    c = score[1]
                else:
                    if score[1] == "A":
                        c = 0.8
                    elif score[1] == "B":
                        c = 0.6
                    elif score[1] == "C":
                        c = 0.4
                    elif score[1] == "D":
                        c = 0.2
                    if len(score) == 3:
                        if score[2] == "-":
                            c = c-0.1
                        elif score[2] == "+":
                            c = c+0.1
                if float(c) > 0.5:
                    polarity = "pos"
                else:
                    polarity = "neg"

        if polarity !="":
            review_list.append((review_text,polarity))

        f = open("Rotten.txt","w")
        for line in review_list:
            f.write(str(line)+"\n")
        f.close()