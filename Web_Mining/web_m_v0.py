'''
웹마이닝 - 다이닝코드사이트에서 추출

'''
# pip install bs4
# pip install requests

import requests
from bs4 import BeautifulSoup

url = "https://www.diningcode.com/list.php?page=1&chunk=10&query=%EA%B0%95%EB%82%A8"

html = requests.get(url)
soup = BeautifulSoup(html.text, 'html.parser')

food_word_list = soup.findAll("div", attrs={"class":"dc-restaurant-category"})

Food_keyword_list = []
for line in food_word_list:
    word_list = line.get_text().split(",")
    for word in word_list:
        Food_keyword_list.append(word.strip())

Food_keyword_list.remove("광고\n키워드중심")
Food_keyword_list.remove("지역기반")
Food_keyword_list.remove("효율높은광고")

print (Food_keyword_list)

Text = ""
for text in Food_keyword_list:
    Text = Text + "," + text