'''
웹마이닝 - 다이닝코드사이트에서 추출

'''
# pip install bs4
# pip install requests

import requests
from bs4 import BeautifulSoup

url=[]
Food_keyword_list = []

for num in range(1,10):
    url = "https://www.diningcode.com/list.php?page="+str(num)+"&chunk=10&query=강남"
    html = requests.get(url)
    soup = BeautifulSoup(html.text, 'html.parser')

    food_word_list = soup.findAll("div", attrs={"class":"dc-restaurant-category"})
    for line in food_word_list:
        word_list = line.get_text().split(",")
        for word in word_list:
            Food_keyword_list.append(word.strip())

##리스트에서 필요없는 단어 삭제
Food_keyword_list.remove("광고\n키워드중심")
Food_keyword_list.remove("지역기반")
Food_keyword_list.remove("효율높은광고")
print (Food_keyword_list)

Text = ""
for text in Food_keyword_list:
    Text = Text + "," + text