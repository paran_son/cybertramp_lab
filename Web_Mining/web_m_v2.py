'''
웹마이닝 - 다이닝코드사이트에서 추출

'''
# pip install bs4
# pip install requests

import requests
from bs4 import BeautifulSoup
from matplotlib import font_manager, rc
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt

url=[]
Food_keyword_list = []

for num in range(1,10):                                                                     # 웹페이지 추출 10번 반복
    url = "https://www.diningcode.com/list.php?page="+str(num)+"&chunk=10&query=일산"
    html = requests.get(url)
    soup = BeautifulSoup(html.text, 'html.parser')

    food_word_list = soup.findAll("div", attrs={"class":"dc-restaurant-category"})
    for line in food_word_list:
        word_list = line.get_text().split(",")
        for word in word_list:
            Food_keyword_list.append(word.strip())

##리스트에서 필요없는 단어 삭제
Food_keyword_list.remove("광고\n키워드중심")
Food_keyword_list.remove("지역기반")
Food_keyword_list.remove("효율높은광고")
print (Food_keyword_list)

# 단어 하나의 리스트로 정리
Text = ""
for text in Food_keyword_list:
    Text = Text + "," + text

stopwords = set(STOPWORDS)
wc = WordCloud(font_path='BMHANNA.ttf',stopwords=stopwords, min_font_size=1, max_font_size=40,background_color='white')
wc.generate_from_text(Text)

plt.figure(figsize=(8,8))
plt.imshow(wc, interpolation='bilinear')
plt.axis("off")
plt.show()